# RPi Grafana In Docker

Running Grafana in your Raspberry Pi in Docker Containers.


## About

I needed a quick repeatable way to launch grafana on all of my rpi's and any
future ones so I can monitor their health easily. This uses `docker-compose` to
launch the entire stack of all apps needed to monitor your rpi with grafana.


## How To

I'll come back and write a more detailed description of getting started. But for
now this is just how to launch this given you have everything setup on your rpi
and installed `docker`, `docker-compose` and cloned this repo.

First, make the changes you want to make to your configs or passwords or environment
variables. The passwords you will want to change are Grafana's login password,
and Influxdb's password. You can find these either in their respected environment
files and/or in their conf files in the respected `./data/` child directory.

Last thing you will want to setup is your `./data/` directory and all the child
directories.

```bash
mkdir -p ./data/{prometheus,grafana,influxdb,telegraf}
```

You may have some permission issues. I need to go back and update this to be
appropriately `chmod`'d and `chown`'d. But you can for now just give them `777`
access and tighten it up later. I think you need to `chown` with the grafana user
id vs user/group name.

Then stand it up


```bash
make up
```

Wow! That was easy!


## Once It's Up

Once everything is running. You will want to initialize your influxdb.

```bash
docker exec -it influxdb influx -password $PASSWORD -username 'admin' -database 'telegraf' -execute 'CREATE RETENTION POLICY "one_month" ON "telegraf" DURATION 30d REPLICATION 1 DEFAULT'
```

This should be scripted later.

Then you will want to add prometheus and influxdb as a data source. You can do
that via the GUI by navigating to `http://<pi-ip>:3000/login` and logging in
with your Grafana login creds.

You might want to copy the `docker-dashboard.json` and import it into your
dashboards to monitor your containers.

There are also a few public ones I favor that give you more rpi metrics specific.
`10578` and `13044`.

